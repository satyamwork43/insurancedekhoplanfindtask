# InsuranceDekhoPlanFindTask

Here's an explanation of the provided code for a README file:

## Overview

This code processes conversation transcripts between agents and customers to extract information such as the insurance plans discussed, the customer's annual salary, and their education level. It utilizes machine learning models for text generation and information extraction.

## Dependencies

- `langchain`
- `transformers`
- `torch`
- `pandas`

Ensure you have the required libraries installed using:
```bash
pip install langchain transformers torch pandas
```

## Functions

### `all_transcript(unique_value)`

This function retrieves and concatenates the transcripts of a given file name.

- **Parameters**: `unique_value` (string): The unique file name.
- **Returns**: The concatenated transcript as a string.

### `extract_text_after_pattern(response, pattern)`

This function extracts text following a given pattern from a response string.

- **Parameters**: 
  - `response` (string): The input text.
  - `pattern` (string): The pattern to search for.
- **Returns**: The text following the pattern or "Pattern not found in the response."

### `extract_salary_info(text)`

This function extracts salary information from a given text based on various patterns.

- **Parameters**: `text` (string): The input text.
- **Returns**: The extracted salary or `None` if no pattern matches.

### `find_plans(text, json_data)`

This function identifies mentioned plans in the given text based on a list of plans in the JSON data.

- **Parameters**: 
  - `text` (string): The input text.
  - `json_data` (list): A list of dictionaries containing plan information.
- **Returns**: A list of mentioned plans.

### `determine_education_level(response)`

This function determines the education level from a given text.

- **Parameters**: `response` (string): The input text.
- **Returns**: The education level or `None` if no match is found.

### `extract_pointers(text, pattern)`

This function extracts pointers from the text based on a given pattern.

- **Parameters**: 
  - `text` (string): The input text.
  - `pattern` (string): The pattern to search for.
- **Returns**: A list of matches.

### `convert_to_lakhs(salary_list)`

This function converts salary values from lakhs to a single unit.

- **Parameters**: `salary_list` (list): A list of salary values in lakhs.
- **Returns**: A list of salary values converted to a single unit.

### `find_plans_all(json_data, education_list, income, target_plans)`

This function identifies available plans based on the user's education and income.

- **Parameters**: 
  - `json_data` (list): A list of dictionaries containing plan information.
  - `education_list` (list): A list of education levels.
  - `income` (float): The customer's income.
  - `target_plans` (list): A list of target plans.
- **Returns**: A string summarizing available and missing plans.

## Main Process

The `process_transcripts()` function processes transcripts, extracts relevant information, and writes missing information to a CSV file.

- **Steps**:
  1. Define the question plan to extract required information.
  2. Retrieve the transcript using the `all_transcript` function.
  3. Split the transcript into chunks for processing.
  4. Use the language model to generate responses for each chunk.
  5. Extract pointers for plans, salary, and education.
  6. Identify mentioned plans, salary, and education.
  7. Summarize the results and write missing information to a CSV file.

### Usage

Run the main process using:
```bash
python your_script_name.py
```

The processed results will be saved in `missing_info.csv`. 

This explanation provides a clear understanding of the code's purpose, dependencies, and functionality. Adjust the paths and file names as needed to fit your environment.

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/satyamwork43/insurancedekhoplanfindtask.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/satyamwork43/insurancedekhoplanfindtask/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
