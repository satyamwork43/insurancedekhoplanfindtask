#!pip install -q torch transformers accelerate bitsandbytes transformers sentence-transformers faiss-gpu
#!pip install -q langchain
#!pip install accelerate


import os

json_data = [
  {
    "Plan": "Max",
    "Minimum Education": "12th",
    "Occupation Type": "Salaried",
    "Minimum Income": "5"
  },
  {
    "Plan": "Max",
    "Minimum Education": "Graduate",
    "Occupation Type": "Salaried",
    "Minimum Income": "3"
  },
  {
    "Plan": "Max",
    "Minimum Education": "Graduate",
    "Occupation Type": "Salaried",
    "Minimum Income": "5"
  },
  {
    "Plan": "Max",
    "Minimum Education": "12th",
    "Occupation Type": "Business",
    "Minimum Income": "5"
  },
  {
    "Plan": "Max",
    "Minimum Education": "Graduate",
    "Occupation Type": "Business",
    "Minimum Income": "5"
  },
  {
    "Plan": "ICICI",
    "Minimum Education": "12th",
    "Occupation Type": "Salaried",
    "Minimum Income": "10"
  },
  {
    "Plan": "ICICI",
    "Minimum Education": "Graduate",
    "Occupation Type": "Salaried",
    "Minimum Income": "3"
  },
  {
    "Plan": "ICICI",
    "Minimum Education": "Graduate",
    "Occupation Type": "Salaried",
    "Minimum Income": "5"
  },
  {
    "Plan": "ICICI",
    "Minimum Education": "12th",
    "Occupation Type": "Salaried",
    "Minimum Income": "10"
  },
  {
    "Plan": "ICICI",
    "Minimum Education": "Graduate",
    "Occupation Type": "Salaried",
    "Minimum Income": "5 "
  },
  {
    "Plan": "ICICI",
    "Minimum Education": "Graduate",
    "Occupation Type": "Salaried",
    "Minimum Income": "10"
  },
  {
    "Plan": "ICICI",
    "Minimum Education": "12th",
    "Occupation Type": "Business",
    "Minimum Income": "10"
  },
  {
    "Plan": "ICICI",
    "Minimum Education": "Graduate",
    "Occupation Type": "Business",
    "Minimum Income": "5"
  },
  {
    "Plan": "ICICI",
    "Minimum Education": "Graduate",
    "Occupation Type": "Business",
    "Minimum Income": "5"
  },
  {
    "Plan": "ICICI",
    "Minimum Education": "12th",
    "Occupation Type": "Business",
    "Minimum Income": "10"
  },
  {
    "Plan": "ICICI",
    "Minimum Education": "Graduate",
    "Occupation Type": "Business",
    "Minimum Income": "8"
  },
  {
    "Plan": "ICICI",
    "Minimum Education": "Graduate",
    "Occupation Type": "Business",
    "Minimum Income": "10"
  },
  {
    "Plan": "ICICI SJB",
    "Minimum Education": "graduation",
    "Occupation Type": "Salaried",
    "Minimum Income": "3"
    },
    {
    "Plan": "ICICI SJB",
    "Minimum Education": "graduation",
    "Occupation Type": "Salaried",
    "Minimum Income": "3"
    },
    {
    "Plan": "ICICI SJB",
    "Minimum Education": "10th",
    "Occupation Type": "Business",
    "Minimum Income": "3"
    },
    {
    "Plan": "ICICI SJB",
    "Minimum Education": "10th",
    "Occupation Type": "Business",
    "Minimum Income": "3"
    },
  {
    "Plan": "HDFC",
    "Minimum Education": "10th",
    "Occupation Type": "Salaried",
    "Minimum Income": "10"
  },
  {
    "Plan": "HDFC",
    "Minimum Education": "12th",
    "Occupation Type": "Salaried",
    "Minimum Income": "5 "
  },
  {
    "Plan": "HDFC",
    "Minimum Education": "Graduate",
    "Occupation Type": "Salaried",
    "Minimum Income": "4.9 "
  },
  {
    "Plan": "HDFC",
    "Minimum Education": "10th",
    "Occupation Type": "Busniess",
    "Minimum Income": "10"
  },
  {
    "Plan": "HDFC",
    "Minimum Education": "12th",
    "Occupation Type": "Busniess",
    "Minimum Income": "5"
  },
  {
    "Plan": "HDFC",
    "Minimum Education": "Graduate",
    "Occupation Type": "Busniess",
    "Minimum Income": "4"
  },
  {
    "Plan": "HDFC",
    "Minimum Education": "10th",
    "Occupation Type": "Salaried",
    "Minimum Income": "0"
  },
  {
    "Plan": "HDFC",
    "Minimum Education": "12th",
    "Occupation Type": "Salaried",
    "Minimum Income": "0"
  },
  {
    "Plan": "HDFC",
    "Minimum Education": "Graduate",
    "Occupation Type": "Salaried",
    "Minimum Income": "0"
  },
  {
    "Plan": "HDFC",
    "Minimum Education": "10th",
    "Occupation Type": "Busniess",
    "Minimum Income": "0"
  },
  {
    "Plan": "HDFC",
    "Minimum Education": "12th",
    "Occupation Type": "Busniess",
    "Minimum Income": "0"
  },
  {
    "Plan": "HDFC",
    "Minimum Education": "Graduate",
    "Occupation Type": "Busniess",
    "Minimum Income": "0"
  },
  {
    "Plan": "Bajaj",
    "Minimum Education": "Graduation",
    "Occupation Type": "Salaried",
    "Minimum Income": "2.5"
  },
  {
    "Plan": "Bajaj",
    "Minimum Education": "Graduation",
    "Occupation Type": "Salaried",
    "Minimum Income": "5"
  },
  {
    "Plan": "Bajaj",
    "Minimum Education": "Graduation",
    "Occupation Type": "Business",
    "Minimum Income": "4"
  },
  {
    "Plan": "Bajaj",
    "Minimum Education": "Graduation",
    "Occupation Type": "Business",
    "Minimum Income": "5"
  },
  {
    "Plan": "Bajaj",
    "Minimum Education": "Graduation",
    "Occupation Type": "Salaried",
    "Minimum Income": "2.5"
  },
  {
    "Plan": "Bajaj",
    "Minimum Education": "Graduation",
    "Occupation Type": "Salaried",
    "Minimum Income": "5"
  },
  {
    "Plan": "Bajaj",
    "Minimum Education": "Graduation",
    "Occupation Type": "Business",
    "Minimum Income": "4"
  },
  {
    "Plan": "Bajaj",
    "Minimum Education": "Graduation",
    "Occupation Type": "Business",
    "Minimum Income": "5"
  },
  {
    "Plan": "aegon",
    "Minimum Education": "10th",
    "Occupation Type": "Salaried",
    "Minimum Income": "2"
  },
  {
    "Plan": "aegon",
    "Minimum Education": "10th",
    "Occupation Type": "Salaried",
    "Minimum Income": "10"
  },
  {
    "Plan": "aegon",
    "Minimum Education": "12th",
    "Occupation Type": "Salaried",
    "Minimum Income": "4.99"
  },
  {
    "Plan": "aegon",
    "Minimum Education": "12th",
    "Occupation Type": "Salaried",
    "Minimum Income": "5"
  },
  {
    "Plan": "aegon",
    "Minimum Education": "Graduate",
    "Occupation Type": "Salaried",
    "Minimum Income": "2"
  },
  {
    "Plan": "aegon",
    "Minimum Education": "Graduate",
    "Occupation Type": "Salaried",
    "Minimum Income": "2.5"
  },
  {
    "Plan": "aegon",
    "Minimum Education": "Graduate",
    "Occupation Type": "Salaried",
    "Minimum Income": "5 "
  },
  {
    "Plan": "aegon",
    "Minimum Education": "10th",
    "Occupation Type": "Business",
    "Minimum Income": "2"
  },
  {
    "Plan": "aegon",
    "Minimum Education": "10th",
    "Occupation Type": "Business",
    "Minimum Income": "10"
  },
  {
    "Plan": "aegon",
    "Minimum Education": "12th",
    "Occupation Type": "Business",
    "Minimum Income": "2"
  },
  {
    "Plan": "aegon",
    "Minimum Education": "12th",
    "Occupation Type": "Business",
    "Minimum Income": "5"
  },
  {
    "Plan": "aegon",
    "Minimum Education": "Graduate",
    "Occupation Type": "Business",
    "Minimum Income": "2"
  },
  {
    "Plan": "aegon",
    "Minimum Education": "Graduate",
    "Occupation Type": "Business",
    "Minimum Income": "4"
  },
  {
    "Plan": "aegon",
    "Minimum Education": "Graduate",
    "Occupation Type": "Business",
    "Minimum Income": "5"
  },
  {
    "Plan": "aegon",
    "Minimum Education": "10th",
    "Occupation Type": "Salaried",
    "Minimum Income": "2"
  },
  {
    "Plan": "aegon",
    "Minimum Education": "10th",
    "Occupation Type": "Salaried",
    "Minimum Income": "10"
  },
  {
    "Plan": "aegon",
    "Minimum Education": "12th",
    "Occupation Type": "Salaried",
    "Minimum Income": "2"
  },
  {
    "Plan": "aegon",
    "Minimum Education": "12th",
    "Occupation Type": "Salaried",
    "Minimum Income": "5"
  },
  {
    "Plan": "aegon",
    "Minimum Education": "Graduate",
    "Occupation Type": "Salaried",
    "Minimum Income": "2"
  },
  {
    "Plan": "aegon",
    "Minimum Education": "Graduate",
    "Occupation Type": "Salaried",
    "Minimum Income": "2.5"
  },
  {
    "Plan": "aegon",
    "Minimum Education": "Graduate",
    "Occupation Type": "Salaried",
    "Minimum Income": "5 "
  },
  {
    "Plan": "aegon",
    "Minimum Education": "10th",
    "Occupation Type": "Business",
    "Minimum Income": "2"
  },
  {
    "Plan": "aegon",
    "Minimum Education": "10th",
    "Occupation Type": "Business",
    "Minimum Income": "10"
  },
  {
    "Plan": "aegon",
    "Minimum Education": "12th",
    "Occupation Type": "Business",
    "Minimum Income": "5"
  },
  {
    "Plan": "aegon",
    "Minimum Education": "12th",
    "Occupation Type": "Business",
    "Minimum Income": "5"
  },
  {
    "Plan": "aegon",
    "Minimum Education": "Graduate",
    "Occupation Type": "Business",
    "Minimum Income": "3.99"
  },
  {
    "Plan": "aegon",
    "Minimum Education": "Graduate",
    "Occupation Type": "Business",
    "Minimum Income": "5"
  },
  {
    "Plan": "aegon",
    "Minimum Education": "Graduate",
    "Occupation Type": "Business",
    "Minimum Income": "5 "
  },
  {
    "Plan": "aegon",
    "Minimum Education": "All Allowed",
    "Occupation Type": "Salaried",
    "Minimum Income": "0"
  },
  {
    "Plan": "aegon",
    "Minimum Education": "All Allowed",
    "Occupation Type": "Business",
    "Minimum Income": "0"
  },
  {
    "Plan": "TATA",
    "Minimum Education": "10th",
    "Occupation Type": "Salaried",
    "Minimum Income": "10"
  },
  {
    "Plan": "TATA",
    "Minimum Education": "12th",
    "Occupation Type": "Salaried",
    "Minimum Income": "5"
  },
  {
    "Plan": "TATA",
    "Minimum Education": "Graduate",
    "Occupation Type": "Salaried",
    "Minimum Income": "5"
  },
  {
    "Plan": "TATA",
    "Minimum Education": "10th",
    "Occupation Type": "Business",
    "Minimum Income": "10"
  },
  {
    "Plan": "TATA",
    "Minimum Education": "12th",
    "Occupation Type": "Business",
    "Minimum Income": "5"
  },
  {
    "Plan": "TATA",
    "Minimum Education": "Graduate",
    "Occupation Type": "Business",
    "Minimum Income": "5"
  },
  {
    "Plan": "TATA",
    "Minimum Education": "10th",
    "Occupation Type": "Salaried",
    "Minimum Income": "15"
  },
  {
    "Plan": "TATA",
    "Minimum Education": "12th",
    "Occupation Type": "Salaried",
    "Minimum Income": "15"
  },
  {
    "Plan": "TATA",
    "Minimum Education": "Graduate",
    "Occupation Type": "Salaried",
    "Minimum Income": "15"
  },
  {
    "Plan": "TATA",
    "Minimum Education": "10th",
    "Occupation Type": "Business",
    "Minimum Income": "15"
  },
  {
    "Plan": "TATA",
    "Minimum Education": "12th",
    "Occupation Type": "Business",
    "Minimum Income": "15"
  },
  {
    "Plan": "TATA",
    "Minimum Education": "Graduate",
    "Occupation Type": "Business",
    "Minimum Income": "15"
  },
  {
    "Plan": "TATA",
    "Minimum Education": "10th",
    "Occupation Type": "Salaried",
    "Minimum Income": "3"
  },
  {
    "Plan": "TATA",
    "Minimum Education": "12th",
    "Occupation Type": "Salaried",
    "Minimum Income": "3"
  },
  {
    "Plan": "TATA",
    "Minimum Education": "Grad",
    "Occupation Type": "Salaried",
    "Minimum Income": "3"
  },
  {
    "Plan": "TATA",
    "Minimum Education": "10th",
    "Occupation Type": "Business",
    "Minimum Income": "3"
  },
  {
    "Plan": "TATA",
    "Minimum Education": "12th",
    "Occupation Type": "Business",
    "Minimum Income": "3"
  },
  {
    "Plan": "TATA",
    "Minimum Education": "Grad",
    "Occupation Type": "Business",
    "Minimum Income": "3"
  },
  {
    "Plan": "PNB",
    "Minimum Education": "12th",
    "Occupation Type": "Salaried",
    "Minimum Income": "3"
  },
  {
    "Plan": "PNB",
    "Minimum Education": "12th",
    "Occupation Type": "Business",
    "Minimum Income": "5"
  },
  {
    "Plan": "CANARA",
    "Minimum Education": "12th",
    "Occupation Type": "Salaried",
    "Minimum Income": "5"
  },
  {
    "Plan": "CANARA",
    "Minimum Education": "12th",
    "Occupation Type": "Salaried",
    "Minimum Income": "5"
  },
  {
    "Plan": "CANARA",
    "Minimum Education": "Graduate",
    "Occupation Type": "Salaried",
    "Minimum Income": "2.5"
  },
  {
    "Plan": "CANARA",
    "Minimum Education": "Graduate",
    "Occupation Type": "Salaried",
    "Minimum Income": "5"
  },
  {
    "Plan": "CANARA",
    "Minimum Education": "12th",
    "Occupation Type": "Business",
    "Minimum Income": "5"
  },
  {
    "Plan": "CANARA",
    "Minimum Education": "12th",
    "Occupation Type": "Business",
    "Minimum Income": "5"
  },
  {
    "Plan": "LIC",
    "Minimum Education": "Graduate",
    "Occupation Type": "Business",
    "Minimum Income": "4"
  }
]

#this is all trans that we have get everytime from Database

import json
from pymongo import MongoClient            
os.environ["CUDA_VISIBLE_DEVICES"] = "0,1"
import re
client = MongoClient('167.71.224.78', 27017)
db = client['insurance_dekho']
collection = db['append_dset']

def all_transcript(unique_value):

    for cursor in collection.find({"file_name":unique_value}):
       # if int(cursor['duration'].split(":")[0]) >= 2:
            stime = time.time()
            all_trans = ""
            for arrow in cursor['paragraphs']:
              all_trans+=arrow['trans']
              all_trans+=" "

            return all_trans



def extract_text_after_pattern(response, pattern):
    # Search for the pattern in the response
    match = re.search(re.escape(pattern), response)
    if match:
        # Extract the text after the pattern
        extracted_text = response[match.end():].strip()
        return extracted_text
    else:
        return "Pattern not found in the response."

pattern = "<|assistant|>"


import locale
locale.getpreferredencoding = lambda: "UTF-8"

from langchain_community.document_loaders.csv_loader import CSVLoader

from langchain.text_splitter import RecursiveCharacterTextSplitter

splitter = RecursiveCharacterTextSplitter(chunk_size=512, chunk_overlap=30)

from langchain.vectorstores import FAISS
from langchain.embeddings import HuggingFaceEmbeddings

import torch
from transformers import AutoTokenizer, AutoModelForCausalLM, BitsAndBytesConfig

model_name = 'stabilityai/stablelm-2-zephyr-1_6b'

bnb_config = BitsAndBytesConfig(
    load_in_4bit=True,
    bnb_4bit_use_double_quant=True,
    bnb_4bit_quant_type="nf4",
    bnb_4bit_compute_dtype=torch.bfloat16
)
model = AutoModelForCausalLM.from_pretrained(model_name, quantization_config=bnb_config,device_map='cpu')

#model = AutoModelForCausalLM.from_pretrained(model_name,device_map='cpu')
tokenizer = AutoTokenizer.from_pretrained(model_name)

import torch
from transformers import AutoModelForCausalLM, AutoTokenizer

from langchain.llms import HuggingFacePipeline
from langchain.prompts import PromptTemplate
from transformers import pipeline
from langchain_core.output_parsers import StrOutputParser
#This is pipeline to load model for text generation
text_generation_pipeline = pipeline(
    model=model,
    tokenizer=tokenizer,
    task="text-generation",
    temperature=0.01,
    do_sample=True,
    repetition_penalty=1.1,
    return_full_text=True,
    max_new_tokens=400,
)

llm = HuggingFacePipeline(pipeline=text_generation_pipeline)

prompt_template = """
<|system|>
Answer the question based on your knowledge. Use the following context to help:

{context}

</s>
<|user|>
{question}
</s>
<|assistant|>

 """

prompt = PromptTemplate(
    input_variables=["context", "question"],
    template=prompt_template,
)

llm_chain = prompt | llm | StrOutputParser()


from langchain_core.runnables import RunnablePassthrough


import re
import time
pattern = r'\{(.*?)\}'

import re
#This is function to get exact salary from trans that model will capture
def extract_salary_info(text):
    # Define patterns for different types of salary representations
    patterns = {
        'average': r'around\s+(\d{1,3}(,\d{3})*?)\s+INR',
        'annual': r'(?:around\s+Rs\.?\s*)?(\d{1,3}(,\d{3})*\d*)',
        'keyword': r'\b(\d{1,3}(,\d{3})*(\.\d+)?)\s*(?:lakhs|crores)?\b',
        'range': r'around\s+(\d{1,3})-(\d{1,3})\sthousand\srupees\sper\smonth',
        'min_max': r'Rs\.\s*([\d,]+)\s*to\s*Rs\.\s*([\d,]+)',
        'lakh': r'(\b(?:one|two|three|four|five|six|seven|eight|nine|\d+)\s+(?:lakh|lakhs?)\b)'
    }

    # Extract salary information based on patterns
    salary_info = {
        'average': None,
        'annual': None,
        'keyword': None,
        'range': None,
        'min_max': None,
        'lakh': None
    }

    for key, pattern in patterns.items():
        matches = re.findall(pattern, text, re.IGNORECASE)
        if matches:
            if key == 'average' or key == 'annual':
                salary_info[key] = int(matches[0][0].replace(',', ''))
            elif key == 'keyword':
                for match in matches:
                    salary_str = match[0].replace(',', '')
                    if salary_str:
                        salary_info[key] = float(salary_str)
                        break  # Stop iterating once a valid salary amount is found
            elif key == 'range':
                lower_bound = int(matches[0][0]) * 1000
                upper_bound = int(matches[0][1]) * 1000
                salary_info[key] = (lower_bound, upper_bound)
            elif key == 'min_max':
                min_salary = int(matches[0][0].replace(',', ''))
                max_salary = int(matches[0][1].replace(',', ''))
                salary_info[key] = (min_salary, max_salary)
            elif key == 'lakh':
                salary_text = matches[0]
                # Extract only the number part
                number_part = re.search(r'\b(?:one|two|three|four|five|six|seven|eight|nine|\d+)\b', salary_text, re.IGNORECASE).group(0)
                if number_part.isdigit():
                    salary_value = int(number_part)
                else:
                    salary_map = {
                        'one': 1, 'two': 2, 'three': 3, 'four': 4, 'five': 5,
                        'six': 6, 'seven': 7, 'eight': 8, 'nine': 9
                    }
                    salary_value = salary_map[number_part.lower()]
                annual_salary = salary_value * 100000
                salary_info[key] = annual_salary

    # Find the first non-None value and return it
    for key, value in salary_info.items():
        if value is not None:
            return value

    return None

import re
#This is function to just get plan name from model text generation
def find_plans(text, json_data):
    mentioned_plans = []
    for item in json_data:
        plan_name_words = item["Plan"].lower().split()  # Split plan name into words
        for word in plan_name_words:
            if re.search(r'\b' + re.escape(word) + r'\b', text.lower()):
                mentioned_plans.append(item["Plan"])
                break
    text = text.replace("bajaj", "")  # Replace "bajaj" with blank
    return mentioned_plans

# find_plans(str(matches),json_data)

tenth_representations = ["10th", "matriculation", "sslc", "secondary school certificate"]
twelfth_representations = ["12th", "plus two", "xii", "higher secondary", "intermediate", "hsc", "class XII"]
graduation_representations = ["graduate", "bachelor's degree", "undergraduate", "baccalaureate", "bsc", "b.a", "b.tech", "b.com", "bs", "bba", "b.e", "llb", "bsc(hons)", "b.a(hons)", "master's degree"]
#this is function to get education from the model text
def determine_education_level(response):
    education_level = None

    # Check for graduation level education
    for keyword in graduation_representations:
        if keyword.lower() in response.lower():
            education_level = "Graduation or Bachelor's degree"
            return education_level

    # Check for twelfth grade education
    for keyword in twelfth_representations:
        if keyword.lower() in response.lower():
            education_level = "Twelfth grade"
            return education_level

    # Check for tenth grade education
    for keyword in tenth_representations:
        if keyword.lower() in response.lower():
            print(keyword,"keyword---------->")
            education_level = "Tenth grade"
            return education_level

    return education_level


import json

# def find_plans_all(json_data, education, income, target_plans):

#     tenth_representations = ["10th", "matriculation", "sslc", "secondary school certificate", "x", "Tenth grade"]
#     twelfth_representations = ["12th", "plus two", "xii", "higher secondary", "intermediate", "hsc", "class XII"]
#     graduation_representations = ["graduation", "bachelor's degree", "undergraduate", "baccalaureate", "bsc", "b.a", "b.tech", "b.com", "bs", "bba", "b.e", "llb", "bsc(hons)", "b.a(hons)", "Graduation or Bachelor's degree"]

#     # Check if education is in tenth_representations
#     if education and education.lower() in [rep.lower() for rep in tenth_representations]:
#         education = "10th"
#     # Check if education is in twelfth_representations
#     elif education and education.lower() in [rep.lower() for rep in twelfth_representations]:
#         education = "12th"
#     elif education and education.lower() in [rep.lower() for rep in graduation_representations]:
#         education = "Graduate"
#     else:
#         education = None

#     available_plans = set()

#     for entry in json_data:
#         try:
#             min_income = float(entry["Minimum Income"])
#             if education and entry["Minimum Education"] == education and min_income <= income:
#                 print("LLLLLLLL")
#                 available_plans.add(entry["Plan"])
#         except (ValueError, KeyError):
#             # Handle cases where Minimum Income cannot be cast to float
#             # or where "Minimum Education" or "Plan" key is not found
#             pass

#     present_plans = set(available_plans)  # Convert available_plans to a set
#     missing_plans = set(target_plans) - present_plans  # Perform set difference operation

#     if missing_plans:
#         missing_str = ", ".join(str(plan) for plan in missing_plans if plan is not None)
#         present_str = ", ".join(present_plans)
#         return f"Available plans: {present_str}. Plans Agent Explained to Customer {target_plans},Plans missing from the available options: {missing_str}"
#     elif present_plans:
#         return "All plans are available"
#     else:
#         return "No plans available"

def extract_pointers(text, pattern):
    matches = re.findall(pattern, text, re.DOTALL)
    # Remove any None values from the matches
    matches = [match for match in matches if match is not None]
    return matches


def convert_to_lakhs(salary_list):
    """
    Convert salary values from lakhs to a single unit.

    Args:
    - salary_list (list): List of salary values in lakhs.

    Returns:
    - list: List of salary values in a single unit.
    """
    converted_salaries = []
    for salary in salary_list:
        # Assuming salary is provided in lakhs, dividing by 100,000 to convert to a single unit
        converted_salaries.append(salary / 100000)
    return converted_salaries

def find_plans_all(json_data, education_list, income, target_plans):
    # Debugging: Print input parameters
    print("Education list:", education_list)
    print("Income:", income)
    print("Target plans:", target_plans)

    # Convert target plans to lowercase and create a set
    target_plans = {plan.lower() for plan in target_plans}

    # Debugging: Print converted target plans
    print("Target plans (lowercase):", target_plans)

    # Check if education list, income, and target plans are provided
    if not education_list or not income or not target_plans:
        print("Something is Missing")
        return 0

    # Mapping of different representations of education levels
    tenth_representations = ["10th", "matriculation", "sslc", "secondary school certificate","Tenth grade"]
    twelfth_representations = ["12th", "plus two", "xii", "higher secondary", "intermediate", "hsc", "class XII"]
    graduation_representations = ["graduation", "bachelor's degree", "undergraduate", "baccalaureate", "bsc", "b.a", "b.tech", "b.com", "bs", "bba", "b.e", "llb", "bsc(hons)", "b.a(hons)", "graduation or bachelor's degree","Graduation or Bachelor's degree"]

    # Initialize set to store available plans
    available_plans = set()

    # Loop through each education level
    for education in education_list:
        # Match education level with representations
        if education.lower() in [rep.lower() for rep in tenth_representations]:
            print(education,"education--------->")
            education = "10th"
        elif education.lower() in [rep.lower() for rep in twelfth_representations]:
            education = "12th"
        elif education.lower() in [rep.lower() for rep in graduation_representations]:
            education = "Graduate"
        else:
            education = None

        # Loop through each entry in JSON data
        for entry in json_data:
            try:
                # Convert minimum income to float
                min_income = float(entry["Minimum Income"])

                # Check if education and income match with entry in JSON data
                if education:
                    if education == "Graduate":
                        # Consider both "10th" and "12th" as acceptable for "Graduate"
                        if entry["Minimum Education"].lower() in ["10th", "12th"] and min_income <= income:
                            available_plans.add(entry["Plan"].lower())
                    elif entry["Minimum Education"].lower() == education.lower() and min_income <= income:
                        available_plans.add(entry["Plan"].lower())
            except (ValueError, KeyError):
                pass

    # Debugging: Print available plans
    print("Available plans:", available_plans)
    print(education, income, "---------------------------------------->>>>>>>>")

    # Calculate present and missing plans
    present_plans = available_plans
    missing_plans = target_plans - present_plans

    # Debugging: Print present and missing plans
    print("Present plans:", present_plans)
    print("Missing plans:", missing_plans)

    # Check if there are missing plans
    if missing_plans:
        missing_str = ", ".join(missing_plans)
        present_str = ", ".join(present_plans)
        return f"Available plans: {present_str}. Plans Agent Explained to Customer {target_plans}, Plans missing from the available options: {missing_str}"
    elif present_plans:
        return "All plans are available"
    else:
        return "No plans available"


question_plan = """
Given the conversation between an agent and a customer from a transcript, extract the following information:

1. Which life insurance plan did the agent explain to the customer?
2. What is the customer's annual salary, if explicitly stated or inferred from the conversation?
3. What is the customer's education? (e.g., high school diploma, bachelor's degree, etc.)
"""

import difflib

import pandas as pd

def process_transcripts():
    stime = time.time()
    results = []
    question_plan = """
    Given the conversation between an agent and a customer from a transcript, extract the following information:
    1. Which life insurance plan did the agent explain to the customer?
    2. What is the customer's annual salary, if explicitly stated or inferred from the conversation?
    3. What is the customer's education? (e.g., high school diploma, bachelor's degree, etc.)
    """
    #all_number = ["1470729", "1474553", "1470670", "1473398"]
    # Regular expression patterns for extracting information
    all_number = ["6637c9524327c3516a88c82d"]
    pattern_plan = r'1\.(.*?)\d\.'
    pattern_salary = r'2\.(.*?)\d\.'
    pattern_education = r'3\.(.*?)(?=\d\.|$)'
    for num in all_number:
        for cursor in collection.find({"phone_number": num}):
            all_trans = all_transcript(cursor['file_name'])
            extracted_text = ""
            chunk_size = 9500

            res = llm_chain.invoke({"context": all_trans, "question": question_plan})
            context_chunks = [all_trans[i:i + chunk_size] for i in range(0, len(all_trans), chunk_size)]
            for i, chunk in enumerate(context_chunks):
                user_prompt = [{'role': 'user', 'content': f"{chunk}\n\n{question_plan}\n\n"}]
                inputs = tokenizer.apply_chat_template(user_prompt, add_generation_prompt=True, return_tensors='pt')
                tokens = model.generate(
                    inputs.to("cpu"),
                    max_new_tokens=356,
                    temperature=0.01,
                    do_sample=True
                )
                response = tokenizer.decode(tokens[0], skip_special_tokens=False)
                response = response.replace("\n", "")
                extracted_text += response

            pointers_starting_with_1 = extract_pointers(extracted_text, pattern_plan)
            pointers_starting_with_2 = extract_pointers(extracted_text, pattern_salary)
            pointers_starting_with_3 = extract_pointers(extracted_text, pattern_education)

            plans_list = []
            salary_list = []
            education_list = []

            for pointer in pointers_starting_with_1:
                plans_list.append(find_plans(pointer.strip(), json_data))

            for pointer in pointers_starting_with_2:
                salary_list.append(extract_salary_info(pointer.strip()))

            for pointer in pointers_starting_with_3:
                education_list.append(determine_education_level(pointer.strip()))

            plans_list = [plan for plan in plans_list if plan is not None]
            plans_list = list(set([plan for sublist in plans_list for plan in sublist]))

            salary_list = [salary for salary in salary_list if salary is not None]
            education_list = [education for education in education_list if education is not None]

            if salary_list:
                salary_list = salary_list[0]
            if education_list:
                education_list = education_list[0]

            unique_plans = plans_list
            if "i gone" in all_trans:
                unique_plans = {"aegon"}
            if "sarol jeevan beema" in all_trans:
                unique_plans = {"icici sjb"}

            if not unique_plans:
                results.append({
                    "phone_number": num,
                    "file_name": cursor['file_name'],
                    "reason": "Plans missing"
                })
            elif not salary_list:
                results.append({
                    "phone_number": num,
                    "file_name": cursor['file_name'],
                    "reason": "Salary missing"
                })
            elif not education_list:
                results.append({
                    "phone_number": num,
                    "file_name": cursor['file_name'],
                    "reason": "Education missing"
                })
    print(time.time()-stime,"------------>Time Taken")
    # Write results to CSV
    df = pd.DataFrame(results)
    df.to_csv('missing_info.csv', index=False)

if __name__ == "__main__":
    process_transcripts()
